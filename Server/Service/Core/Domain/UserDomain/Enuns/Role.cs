﻿namespace Domain.UserDomain.Enuns
{
    public enum Role
    {
        Manager = 0,
        Articulator = 1,
        Participant = 2
    }
}
