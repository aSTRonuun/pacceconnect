﻿namespace Application.Utils
{
    public enum ErrorCodes
    {
        ENTITY_MISSING_REQUIRED_INFORMATION = 1,
        ENTITY_NOT_FOUND = 2,
        ENTITY_ALREADY_EXISTS = 3,
        ENTITY_INVALID_INFORMATION = 4,
        UNABLE_TO_ATHENTICATE_USER = 5,
        USER_PASSWORD_INCORRECT = 6,
        USER_NOT_FOUND = 7,
    }
}
